% function to encode spike data into binary event format represented by 80
% bit number
% the first 16 bits (bits 79-64) represent the neuronID
% next 24 bits (bits 63-40) represents the start time in microseconds
% next 24 bits (bits 39-16) represents the end time in microseconds
% the last 16 bits (bits 15-0) represents the spike event timestamp in
% microseconds
% 
% Usage:
% encode1DNumSpikes(<name fo spike data file>, <rangeStamp = [neuronID, startTimeID(ms), endTimeID(ms), numSpikes]>)
function encode1DNumSpikes(filename, rangeStamp)
neuronID = uint16(rangeStamp(:, 1)) - 1;
startID  = uint32(rangeStamp(:, 2)*1000);
endID    = uint32(rangeStamp(:, 3)*1000);
numSpikes= uint16(rangeStamp(:, 4));

rangeStream = [bitshift(bitand(neuronID(:)', hex2dec('FF00')), -8)
                        bitand(neuronID(:)', hex2dec('00FF'))
               bitshift(bitand(startID(:)'  , hex2dec('FF0000')), -16)
               bitshift(bitand(startID(:)'  , hex2dec('FF00')), -8)
                        bitand(startID(:)'  , hex2dec('FF'))
               bitshift(bitand(endID(:)'  , hex2dec('FF0000')), -16)
               bitshift(bitand(endID(:)'  , hex2dec('FF00')), -8)
                        bitand(endID(:)'  , hex2dec('FF'))
               bitshift(bitand(numSpikes(:)', hex2dec('FF00')), -8)
                        bitand(numSpikes(:)', hex2dec('00FF'))
                        ];

rangeStream = uint8(rangeStream);

fid = fopen(filename, 'wb');
fwrite(fid, rangeStream);
fclose(fid);