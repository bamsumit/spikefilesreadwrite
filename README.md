# README #

This git contains `MATLAB` scripts to read and write spike data in binary format.
There are three binary spike data format `*.bs1`, `*.bs2` and `*.bsn` as described below

|File     |Description                          |  
|:-------:|:------------------------------------|  
|`*.bs1`  | 1 dimensional binary spike file     |    
|`*.bs2`	| 2 dimensional binary spike file     |    
|`*.bsn`	| number of spikes file     	      |   
	  
In addition, there are `MATLAB` scripts to read spike event data from __jAER__ files from __DAVIS__ sensor.
* For jAER2.0 or below, the scripts in `jAER matlab scripts/`
* For the scripts in `jAER matlab scripts Aedat3.0 +/` support newer jAER3.0 or higher as well. 

The binary spike files are represented as described below.

### `*.bs1` file
* Each spike event is represented by a 40 bit number.
* First 16 bits (bits 39-24) represent the neuronID.
* Bit 23 represents the sign of spike event: 0=>OFF event, 1=>ON event.
* the last 23 bits (bits 22-0) represent the spike event timestamp in microseconds.

### `*.bs2` file
* It is the same format used in neuromorphic datasets NMNIST & NCALTECH101.
* Each spike event is represented by a 40 bit number.
* First 8 bits (bits 39-32) represent the xID of the neuron.
* Next 8 bits (bits 31-24) represent the yID of the neuron.
* Bit 23 represents the sign of spike event: 0=>OFF event, 1=>ON event.
* The last 23 bits (bits 22-0) represent the spike event timestamp in microseconds.

### `*.bsn` file
* Number of spikes data is represented by an 80 bit number
* First 16 bits (bits 79-64) represent the neuronID
* Next 24 bits (bits 63-40) represents the start time in microseconds
* Next 24 bits (bits 39-16) represents the end time in microseconds
* Last 16 bits (bits 15-0) represents the spike event timestamp in microseconds