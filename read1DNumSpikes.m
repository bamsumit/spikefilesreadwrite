% function to read spike data in binary event format represented by 80
% bit number
% the first 16 bits (bits 79-64) represent the neuronID
% next 24 bits (bits 63-40) represents the start time in microseconds
% next 24 bits (bits 39-16) represents the end time in microseconds
% the last 16 bits (bits 15-0) represents the spike event timestamp in
% microseconds
% 
% Usage:
% [neuronID, startTimeID(ms), endTimeID(ms), numSpikes] = read1DNumSpikes(filename)
% timeID is in ms
function [neuronID, startID, endID, numSpikes] = read1DBinSpikes(filename)
% filename = 'spikesN250T250.bin';
rangeData = fopen(filename);
rangeStream = fread(rangeData);
fclose(rangeData);

neuronID = bitshift(rangeStream(1:10:end), 8) + rangeStream(2:10:end) + 1;
startID  = bitshift(rangeStream(3:10:end), 16) + ...
           bitshift(rangeStream(4:10:end), 8) + ...
                    rangeStream(5:10:end);
endID    = bitshift(rangeStream(6:10:end), 16) + ...
           bitshift(rangeStream(7:10:end), 8) + ...
                    rangeStream(8:10:end);
numSpikes= bitshift(rangeStream(9:10:end), 8) + rangeStream(10:10:end);
startID = startID/1000;
endID   = endID/1000;
