clear all;

% XOR
data = cell(2, 1);
%neuronID, startTimeID(ms), endTimeID(ms), numSpikes
data{1} = [ 1 0 6 2];   % data 0
data{2} = [ 1 0 6 10];  % data 1
        
for i=1:length(data)
    filename = [num2str(i-1) '.bsn'];
    encode1DNumSpikes(filename, data{i});
end

% NMNIST
% data = cell(10, 1);
% %neuronID, startTimeID(ms), endTimeID(ms), numSpikes
% 
% numSpikes = 10*ones(10) + 50*eye(10);
% for i=1:length(data)
%     data{i}  = [(1:10)' repmat([0 300], [10 1]) numSpikes(:, i)];
%     filename = [num2str(i-1) '.bsn'];
%     encode1DNumSpikes(filename, data{i});
% end