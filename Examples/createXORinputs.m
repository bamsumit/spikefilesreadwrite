clear all;
zero = [0 2.5];
one  = 0:0.5:5;

data = cell(4, 1);
data{1} = [1*ones(size(zero))   2*ones(size(zero));
                       zero                 zero;
            ones(size(zero))      ones(size(zero))]';
data{2} = [1*ones(size(zero))   2*ones(size(one));
                       zero                 one;
            ones(size(zero))      ones(size(one))]';
data{3} = [1*ones(size(one))   2*ones(size(zero));
                       one                 zero;
            ones(size(one))      ones(size(zero))]';
data{4} = [1*ones(size(one))   2*ones(size(one));
                       one                 one;
            ones(size(one))      ones(size(one))]';
for i=1:length(data)
    filename = [num2str(i-1) '.bs1'];
    encode1DBinSpikes(filename, data{i});
end