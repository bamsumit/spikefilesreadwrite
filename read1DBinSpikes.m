% function to read 1D binary spike event representation
% The first 16 bits (bits 39-24) represent the neuronID
% bit 23 represents the sign of spike event: 0=>OFF event, 1=>ON event
% the last 23 bits (bits 22-0) represent the spike event timestamp in
% microseconds
% 
% Usage:
% [neuronID, timeID, polarity] = read1DBinSpikes(filename)
% timeID is in ms
function [neuronID, timeID, polarity] = read1DBinSpikes(filename)
% filename = 'spikesN250T250.bin';
eventData = fopen(filename);
eventStream = fread(eventData);
fclose(eventData);

neuronID = bitshift(eventStream(1:5:end), 8) + eventStream(2:5:end) + 1;
polarity = bitshift(eventStream(3:5:end),-7);
timeID   = bitshift(bitand(eventStream(3:5:end), hex2dec('7F')), 16) + ...
           bitshift(       eventStream(4:5:end), 8) + ...
                           eventStream(5:5:end);
timeID   = timeID/1000.0;
